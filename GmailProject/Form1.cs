﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Google.Apis.Services;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using System.Net;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Util.Store;
using System.IO;
using System.Threading;
using System.Net.Mail;

namespace Gmail_Project
{

    public partial class Form1 : Form
    {
        private GmailService service = new GmailService();
        private String currentEmail = null;

        public Form1()
        {
            string[] Scopes = { GmailService.Scope.MailGoogleCom, GmailService.Scope.GmailSend, GmailService.Scope.GmailCompose, GmailService.Scope.GmailModify };
            string ApplicationName = "Gmail";
            UserCredential credential;

            using (var stream =
                new FileStream("client_secret.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/gmail-dotnet-quickstart.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            // create Gmail API service.
            service = new GmailService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });

            UsersResource.GetProfileRequest request = service.Users.GetProfile("me");
            currentEmail = request.Execute().EmailAddress;

            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) // list all mails
        {
            
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List("me");
            List<Google.Apis.Gmail.v1.Data.Message> result = new List<Google.Apis.Gmail.v1.Data.Message>(); // laiškų sąrašas

            do
            {
                try
                {
                    ListMessagesResponse response = request.Execute();
                    result.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occurred: " + ex.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));

            foreach (var element in result)
            {
                inbox.Items.Add(service.Users.Messages.Get("me", element.Id).Execute().Snippet);
            }

        }

        private void button1_Click_1(object sender, EventArgs e) // add file
        {
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                attachment.Text = openFileDialog1.FileName.ToString();
            }

        }


        private void button2_Click(object sender, EventArgs e) // send message
        {
            try
            {
                var msg = new System.Net.Mail.MailMessage
                {
                    Subject = subject.Text,
                    Body = body.Text,
                    From = new MailAddress(currentEmail),
                };

                msg.To.Add(new MailAddress(receiver.Text));
                msg.ReplyToList.Add(msg.From);
                msg.Subject = msg.Subject;
                msg.Body = msg.Body;

                if (attachment.Text != null)
                {
                    msg.Attachments.Add(new Attachment(attachment.Text));
                }

                var mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(msg);

                var gmailMessage = new Google.Apis.Gmail.v1.Data.Message
                {
                    Raw = Encode(mimeMessage.ToString())
                };

                service.Users.Messages.Send(gmailMessage, "me").Execute();

                MessageBox.Show("Jūsų žinutė sėkmingai išsiųsta!");
                receiver.Text = "";
                subject.Text = "";
                attachment.Text = "";
                body.Text = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                Console.WriteLine("An error occurred: " + ex.Message);
            }

        }
        public static string Encode(string text)
        {
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);

            return System.Convert.ToBase64String(bytes)
                .Replace('+', '-')
                .Replace('/', '_')
                .Replace("=", "");
        }


        private void button3_Click(object sender, EventArgs e) // open message
        {
            
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List("me");
            List<Google.Apis.Gmail.v1.Data.Message> result = new List<Google.Apis.Gmail.v1.Data.Message>();

            do
            {
                try
                {
                    ListMessagesResponse response = request.Execute();
                    result.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Klaida: " + ex.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));

            try {

                tabControl1.SelectTab(tabPage3);

                int i = 0;
                foreach (var element in result)
                {
                    if (inbox.SelectedIndex == i)
                    {
                        tabControl1.SelectTab(tabPage4);
                        textBox1.Text = Base64UrlDecode(service.Users.Messages.Get("me", element.Id).Execute().Payload.Parts[0].Body.Data);
                        break;
                    }
                    else { i++; }
                }

            }   catch (Exception ex) { Console.WriteLine("Klaida: " + ex.Message);  } 
            
        }

        public static string Base64UrlDecode(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
                return "<strong>Message body was not returned from Google</strong>";

            string InputStr = input.Replace("-", "+").Replace("_", "/");
            return System.Text.Encoding.UTF8.GetString(Convert.FromBase64String(InputStr));

        }


        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e) // profile information
        {
            UsersResource.GetProfileRequest request = service.Users.GetProfile("me");
            UsersResource.LabelsResource.GetRequest request2 = service.Users.Labels.Get("me", "UNREAD");

            currentEmail = request.Execute().EmailAddress;
            int? msgTotal = request.Execute().MessagesTotal;
            int? unread = request2.Execute().MessagesUnread;

            MessageBox.Show(" Jūsų el. pašto adresas: " + currentEmail + "\n Jūsų pašto dėžutėje yra laiškų: " + 
                msgTotal.ToString() + "\n Neperskaitytų laiškų: " + unread.ToString());

        }

        private void apieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Jūs naudojatės paprasta Gmail aplikacija.\n" +
                "Programėlė leidžia peržiūrėti pašto dėžutės turinį, trinti pasirinktus laiškus, " +
                "bei siųsti elektroninius laiškus.");

        }

        private void išeitiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void inboxLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl1.SelectTab(tabPage2);
        }

        private void emailLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            tabControl1.SelectTab(tabPage3);
        }

        private void delete_Click(object sender, EventArgs e) // delete message
        {
            UsersResource.MessagesResource.ListRequest request = service.Users.Messages.List("me");
            List<Google.Apis.Gmail.v1.Data.Message> result = new List<Google.Apis.Gmail.v1.Data.Message>();

            do
            {
                try
                {
                    ListMessagesResponse response = request.Execute();
                    result.AddRange(response.Messages);
                    request.PageToken = response.NextPageToken;

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Klaida: " + ex.Message);
                }
            } while (!String.IsNullOrEmpty(request.PageToken));


            try
            {
                int i = 0;
                foreach (var element in result)
                {
                    if (inbox.SelectedIndex == i)
                    {
                        service.Users.Messages.Delete("me", element.Id).Execute();
                        MessageBox.Show("Žinutė sėkmingai ištrinta");
                        inbox.Items.RemoveAt(i);
                        break;
                    }
                    else { i++; }
                }

            }
            catch (Exception ex) { MessageBox.Show("Klaida: " + ex.Message); }       
        }

    }
}
